// Stolen from stackoverflow (http://stackoverflow.com/questions/860339/difference-between-private-public-and-protected-inheritance)
// Don't try to compile this - it's just for reference

class A 
{
    public:
    // public variables *are* accessible from outside the class
        int x;
    protected: 
    // protected variables *not* accessible from outside the class (like private variables)
    // but *are* inherited by derived classes (unlike private variables)
        int y;
    private:
    // private variables are *not* accessible from outside the class
        int z;
};

class B : public A
{
    // x is public
    // y is protected
    // z is not accessible from B
};

class C : protected A
{
    // x is protected
    // y is protected
    // z is not accessible from C
};

class D : private A
{
    // x is private
    // y is private
    // z is not accessible from D
};