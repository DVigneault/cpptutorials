#include <iostream>

// This is a base class 
class myBaseClass
{
	public: 
		myBaseClass(int x) : myBaseClassVar(x) {};
		void myBaseFunc() 
		{
			std::cout << "The base func calling, the value is " << myBaseClassVar << std::endl;
		} 
		virtual void myVirtualBaseFunc() 
		{
			std::cout << "The virtual func calling, the value is " << myBaseClassVar << std::endl;
		} 
		// A pure virtual function (uncomment this and you'll see it won't compile)
		//virtual void myPureVirtualFunc() =0;

	protected: // protected members are like private members, but are inherited by "children"
		int myBaseClassVar;
};

// This is a class that is derived from the base class - i.e. inherits all its characteristics
class myDerivedClass : public myBaseClass
{
	public: 
		myDerivedClass(int x, int y) : myBaseClass(x), myDerivedClassVar(y) {};
		void myDerivedFunc() 
		{
			std::cout << "The derived func calling, the values are " << myBaseClassVar << " and " << myDerivedClassVar << std::endl;
		} 
		// comment this out to see how the behaviour changes
		void myVirtualBaseFunc()
		{
			std::cout << "An overidden function from the derived class, the value is " << myDerivedClassVar << std::endl;
		}

	protected: 
		int myDerivedClassVar;
};



int main()
{
	// The base (in this case) is just a normal class
	std::cout << "Function calls to the base class: " << std::endl;
	myBaseClass B(1);
	B.myBaseFunc();
	B.myVirtualBaseFunc();

	// The derived class has all the members (data and methods) 
	// of the base class, as well as its own
	std::cout << std::endl << "Function calls to the derived class: " << std::endl;
	myDerivedClass D(2,3);
	D.myDerivedFunc(); 
	D.myBaseFunc();
	D.myVirtualBaseFunc();

	// Polymorphism - a derived class can be accessed through a pointer
	// a base class

	// Two pointers to the derived class instance D, one base class pointer 
	// and one derived class pointer
	myBaseClass* base_ptr = &D;
	myDerivedClass* derived_ptr = &D;

	// We can access the whole object through the derived pointer
	std::cout << std::endl << "Through the derived pointer" << std::endl;
	derived_ptr->myDerivedFunc();
	derived_ptr->myBaseFunc();
	derived_ptr->myVirtualBaseFunc();

	// Through the base pointer we can only access those members that are inherited 
	// from the base class including the virtual functions (even if these are overidden)
	std::cout << std::endl << "Through the base pointer" << std::endl;
	// base_ptr->myDerivedFunc(); // this won't compile if you uncomment it
	base_ptr->myBaseFunc();
	base_ptr->myVirtualBaseFunc(); // note that the overidden function is called (unless you comment it out)

}