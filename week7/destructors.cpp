#include <string>
#include <iostream>
#include <assert.h>

class image 
{
	public: 
		image(); // default constructor
		image(const int x, const int y); // constructor with dimensions
		void readFromFile(const std::string filename);
		int getPixel(const int x, const int y);
		~image(); // destrcutor

	private:
		int xsize, ysize, numPixels;
		int* pixels;
		int sub2ind(int x, int y);

};

// Default constructor - set sensible default values
image::image() : xsize(0), ysize(0), pixels(NULL), numPixels(0)
{}

// Constructor with dimensions
image::image(int const x, int const y) : xsize(x), ysize(y), numPixels(x*y)
{
	// Allocate memory for the pixels
	pixels = new int[xsize*ysize];

	// Fill with zeros
	for(int i; i < numPixels; ++i)
		pixels[i] = 0;
}

int image::sub2ind(const int x, const int y)
{
	return y*xsize + x;
}

// Returns the value of a pixel at a given position
int image::getPixel(const int x, const int y)
{
	// Bounds check
	assert( (x >= 0) && (x < xsize) && (y >= 0) && (y < ysize));

	// return the relevant pixel's value
	return pixels[sub2ind(x,y)];
}

// Destructor - this is where we clean up memory we have used
image::~image()
{
	std::cout << "In the object destructor" << std::endl;
	if(pixels != NULL)
		delete[] pixels;
}

void image::readFromFile(const std::string filename)
{
	// Some code to read in a file and set pixel values
}


// Main Function
int main()
{
	// Create an image object using the consructor with dimensions
	image myStackImage(256,256);

	std::cout << myStackImage.getPixel(128,128) << std::endl; 

	// Dynamically allocate an image object
	image* myHeapImage_ptr = new image;  

	std::cout << "Deleting the dynamic image" << std::endl;
	delete myHeapImage_ptr;

	std::cout << "At the end of int main" << std::endl;

}