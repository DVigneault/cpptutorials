#include <iostream>
#include <cmath>

// This file demonstrates a small 3d vector class and how to override operators that act on class instances
// For a detailed explanation of why the operator overloads look the way they do, see
// http://courses.cms.caltech.edu/cs11/material/cpp/donnie/cpp-ops.html

class vector3d
{
	public: 
		vector3d(); // default constructor
		vector3d(double x, double y, double z); // constructor with elements
		void setElements(double x, double y, double z); // constructor with elements
		vector3d& operator+= (const vector3d& a); // compound addition operator
		vector3d operator+(const vector3d &other) const ; // binary addition operator
		double norm() const; // find the vector norm
		void print() const; // print the vector contents

	private:
		double elements[3]; // the three elements of the vector
};

// Default Constructor
vector3d::vector3d() 
{
	this->elements[0] = 0.0;
	this->elements[1] = 0.0;
	this->elements[2] = 0.0;

}

// Constructor with elements 
vector3d::vector3d(double x, double y, double z)
{
	this->elements[0] = x;
	this->elements[1] = y;
	this->elements[2] = z;

}

// set elements
void vector3d::setElements(double x, double y, double z)
{
	this->elements[0] = x;
	this->elements[1] = y;
	this->elements[2] = z;
}

// compound addition operator 
vector3d& vector3d::operator+= (const vector3d& rhs)
{
	// Add the corresponding rhs elements
	this->elements[0] += rhs.elements[0];
	this->elements[1] += rhs.elements[1];
	this->elements[2] += rhs.elements[2];

	// Return a refernce to self
	return *this;
}

// binary addition operator - add one vector to the other and return
// the result
vector3d vector3d::operator+(const vector3d &other) const 
{
	// We can use a shortcut here and use the compound addition operator that we already define
    vector3d result = *this;  // NB copy constructor called here   
    result += other; // use the compound operator          
    return result;   
}

// Norm - a standard member function. Return the vector Euclidean norm
double vector3d::norm() const
{
	return sqrt(elements[0]*elements[0] + elements[1]*elements[1] + elements[2]*elements[2]);
}

// Print out the vector contents in square brackets
void vector3d::print() const
{
	std::cout << "[ " << this->elements[0] << " " << this->elements[1] << " " << this->elements[2] << " ]" << std::endl;
}

int main()
{
	// Declare and initialise two vectors
	vector3d a_vec(3,4,5);
	vector3d b_vec(1,2,3);
	vector3d c_vec;

	// Do some addition operations
	a_vec += b_vec;
	c_vec = a_vec + b_vec;

	// Print output
	a_vec.print();
	b_vec.print();
	c_vec.print();
}