// ITK Headers
#include "itkTranslationTransform.h"
#include "itkEuclideanDistancePointMetric.h"
#include "itkLevenbergMarquardtOptimizer.h"
#include "itkPointSetToPointSetRegistrationMethod.h"
#include "itkMesh.h"
#include "itkMeshFileReader.h"

// STD Headers
#include <iostream>

// Typedefs
const unsigned int Dimension = 3;
typedef float TPixel;

typedef itk::Mesh< TPixel, Dimension >                            TMesh;
typedef itk::MeshFileReader< TMesh >                              TReader;
typedef itk::EuclideanDistancePointMetric< TMesh, TMesh>          TMetric;
typedef itk::TranslationTransform< double, Dimension >            TTransform;
typedef itk::LevenbergMarquardtOptimizer                          TOptimizer;
typedef itk::PointSetToPointSetRegistrationMethod< TMesh, TMesh > TRegistration;

int
main(int argc, char ** argv )
{

  // Make sure the correct number of arguments was supplied
  if (argc != 3)
    {
    std::cerr
      << "Usage: " << argv[0] << " <fixedPointsFile>  <movingPointsFile>"
      << std::endl;
    return EXIT_FAILURE;
    }

  // Read the fixed mesh
  TReader::Pointer fixedReader = TReader::New();
  fixedReader->SetFileName( argv[1] );
  try
    {
    fixedReader->Update();
    }
  catch( itk::ExceptionObject & e )
    {
    std::cout << e << std::endl;
    return EXIT_FAILURE;
    }

  // Read the moving mesh
  TReader::Pointer movingReader = TReader::New();
  movingReader->SetFileName( argv[2] );
  try
    {
    movingReader->Update();
    }
  catch( itk::ExceptionObject & e )
    {
    std::cout << e << std::endl;
    return EXIT_FAILURE;
    }

  // Metric, Transform, and Optimizer
  TMetric::Pointer metric = TMetric::New();

  TTransform::Pointer transform = TTransform::New();
  transform->SetIdentity();

  TOptimizer::Pointer optimizer = TOptimizer::New();
  optimizer->SetUseCostFunctionGradient(false);
  optimizer->SetNumberOfIterations( 100 );
  optimizer->SetEpsilonFunction( 1e-6 );

  // Registration Method
  TRegistration::Pointer registration = TRegistration::New();
  registration->SetInitialTransformParameters( transform->GetParameters() );
  registration->SetMetric(                     metric                     );
  registration->SetOptimizer(                  optimizer                  );
  registration->SetTransform(                  transform                  );
  registration->SetFixedPointSet(              fixedReader->GetOutput()   );
  registration->SetMovingPointSet(             movingReader->GetOutput()  );

  try
    {
    registration->Update();
    }
  catch( itk::ExceptionObject & e )
    {
    std::cout << e << std::endl;
    return EXIT_FAILURE;
    }

  std::cout << "Solution = " << transform->GetParameters() << std::endl;

  return EXIT_SUCCESS;

}

