// Include the cout and cin functionality
#include <iostream> 


// Start of the main function
int main()
{
	// Allocate memory for three integers
	int x,y,z;

	// Get two integers from the user
	// NB no error checking here
	std::cout << "Enter the first number: " << std::endl << ">> ";
	std::cin >> x;
	std::cout << "Enter the second number: " << std::endl << ">> ";
	std::cin >> y;

	// Perform operations on the integers and output the results

	// Addition
	z = x + y;
	std::cout << "x + y = " << z << std::endl;

	// Subraction
	z = x - y; 
	std::cout << "x - y = " << z << std::endl;
	
	// Multiplication
	z = x * y;
	std::cout << "x * y = " << z << std::endl;

	// (Integer) Division 
	z = x / y; 
	std::cout << "x / y = " << z << std::endl;

	// Modulo (remainder)
	z = x % y; 
	std::cout << "x modulo y = " << z << std::endl; 


} // end of the main function 