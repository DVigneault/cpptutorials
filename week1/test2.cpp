// Include the cout and cin functionality
#include <iostream>
// Include the atoi functions
#include <cstdlib>

// Start of the main function
int main(int argc, char** argv)
{
	// Allocate memory for three integers
	int x,y,z;

	// Get two integers from the user
	// NB no error checking here
	x = atoi(argv[1]);
	y = atoi(argv[2]);

	// Perform operations on the integers and output the results

	// Addition
	z = x + y;
	std::cout << "x + y = " << z << std::endl;

	// Subraction
	z = x - y; 
	std::cout << "x - y = " << z << std::endl;
	
	// Multiplication
	z = x * y;
	std::cout << "x * y = " << z << std::endl;

	// (Integer) Division 
	z = x / y; 
	std::cout << "x / y = " << z << std::endl;

	// Modulo (remainder)
	z = x % y; 
	std::cout << "x modulo y = " << z << std::endl; 


} // end of the main function 