#include <iostream> // cout, endl
#include <cstdlib> // malloc(), free()

using namespace std;

// Prototype
void doubleAnInt(int* anInt);

int main()
{
	// POINTERS
	// --------

	// Two ints 
	int x = 7;
	int y = 8;

	// Two int pointers
	// Set to addresses of x and y
	int* a_ptr = &x; // create a (pointer to an int) called a_ptr, and set its value to be the address of the local variable x 
	int* b_ptr = &y; // create a (pointer to an int) called b_ptr, and set its value to be the address of the local variable y 

	// Output stuff
	// Here we see that a_ptr and b_ptr are addresses, and by dereferencing them with '*' we see to value of the int they point to 
	cout << "x = " << x << ", &x = " << &x << endl;
	cout << "y = " << y << ", &y = " << &y << endl;
	cout << "a_ptr = " << a_ptr << ", *a_ptr = " << *a_ptr << endl; 
	cout << "b_ptr = " << b_ptr << ", *b_ptr = " << *b_ptr << endl << endl; 

	// Swap the pointers
	a_ptr = &y; // set the value of a_ptr to be the address of the local variable y 
	b_ptr = &x; // set the value of a_ptr to be the address of the local variable x

	// Output again
	// Here we see that the values of x and y remain unchanged by the swap, but the addresses stored in the pointers swap, and therefore so do their dereferenced values
	cout << "x = " << x << ", &x = " << &x << endl;
	cout << "y = " << y << ", &y = " << &y << endl;
	cout << "a_ptr = " << a_ptr << ", *a_ptr = " << *a_ptr << endl; 
	cout << "b_ptr = " << b_ptr << ", *b_ptr = " << *b_ptr << endl << endl; 

	// Make assignments to the variables
	x = 879; // set the value of the int x to 879
	y = -986; // set the value of the int y to -986

	// Output again
	// Here we see that we can change the values of x and y using their local variable names, 
	// the addresses of the pointers do no change, but the dereferenced values do 
	cout << "x = " << x << ", &x = " << &x << endl;
	cout << "y = " << y << ", &y = " << &y << endl;
	cout << "a_ptr = " << a_ptr << ", *a_ptr = " << *a_ptr << endl; 
	cout << "b_ptr = " << b_ptr << ", *b_ptr = " << *b_ptr << endl << endl; 

	// Make assignments through the pointers
	*a_ptr = 67; // changes the value of the int pointed to by a_ptr (currently the local variable y) to 67
	*b_ptr = *a_ptr; // finds the int stored at the address in a_ptr (which is currently 67) and stores the result in the int that b_ptr points to

	// Output again
	cout << "x = " << x << ", &x = " << &x << endl;
	cout << "y = " << y << ", &y = " << &y << endl;
	cout << "a_ptr = " << a_ptr << ", *a_ptr = " << *a_ptr << endl; 
	cout << "b_ptr = " << b_ptr << ", *b_ptr = " << *b_ptr << endl << endl;  

	// REFERENCES
	// ----------

	// A reference
	int& x_ref = x; // bind the reference to the local variable x

	// Output
	// Here we see that x and the target of x_ref realy do hold the same space
	// in memory and therefore have the same value
	cout << "x = " << x << ", &x = " << &x << endl;
	cout << "x_ref = " << "&x_ref = " << &x_ref << endl;

	// Change using the original variable
	x = 99; // set the value of the local variable x to 

	// Output
	// Here we see that updating x updates the value that x_ref sees too
	cout << "x = " << x << ", &x = " << &x << endl;
	cout << "x_ref = " << x_ref << "&x_ref = " << &x_ref << endl << endl;

	// Change using the reference
	x_ref = 78; // set the int that x_ref refers to be 78

	// Output
	// Here we see that updating x_ref updates the value that x sees too
	cout << "x = " << x << ", &x = " << &x << endl;
	cout << "x_ref = " << x_ref << "&x_ref = " << &x_ref << endl << endl;

	// However once bound, a reference cannot be bound to anything else
	x_ref = y; // This does NOT make x_ref refer to y. Instead it copies the value found in y to the integer that x_ref refers to (i.e. x)

	// Output
	// Here we see that x_ref still shares the memory of x, and that we have copied the value of y into there
	cout << "x = " << x << ", &x = " << &x << endl;
	cout << "x_ref = " << x_ref << ", &x_ref = " << &x_ref << endl << endl;


	// DYNAMIC MEMORY ALLOCATION
	// -------------------------

	// Declare a new pointer to an int 
	// Set it to NULL (always good practice)
	int* dynamic_ptr = NULL;

	// Show that the NULL pointer is zero
	cout << "dynamic_ptr = " << dynamic_ptr << endl;

	// Create a dynamic array the C++ way
	#define ARRAY_SIZE 5
	dynamic_ptr = new int[ARRAY_SIZE];

	// Show that the pointer has been given some memory somwhere
	cout << "dynamic_ptr = " << dynamic_ptr << endl << endl;

	// Show that the dynamically allocated array of ints in uninitialised
	cout <<  "The array at dynamic_ptr is " ;
	for(int i = 0; i < ARRAY_SIZE; ++i)
		cout << dynamic_ptr[i] << " ";
	cout << endl << endl;

	// Put some values into the array
	for(int i = 0; i < ARRAY_SIZE; ++i)
		dynamic_ptr[i] = 10+i;

	// Output the contents
	cout <<  "The array at dynamic_ptr is " ;
	for(int i = 0; i < ARRAY_SIZE; ++i)
		cout << dynamic_ptr[i] << " ";
	cout << endl << endl;

	// Pointer arithmetic
	int* offset_ptr = dynamic_ptr + 3; // move along 3 int's worth of memory (normally 12 bytes)

	// Show that offset pointer points to the third element of the array
	cout << "dynamic_ptr = " << dynamic_ptr << ", *dynamic_ptr = " << *dynamic_ptr << endl ;
	cout << "offset_ptr = " << offset_ptr << ", *offset_ptr = " << *offset_ptr << endl ;

	// Show that the [] operator on a pointer is really just pointer arithmetic
	cout << "&(dynamic_ptr[4]) = " << &(dynamic_ptr[4]) << ", dynamic_ptr+4 = " << dynamic_ptr+4 << endl ;
	cout << "dynamic_ptr[4] = " << dynamic_ptr[4] << ", *(dynamic_ptr+4) = " << *(dynamic_ptr+4) << endl << endl;

	// For every new, put a delete!!!
	// For every new, put a delete!!!
	// For every new, put a delete!!!
	// For every new, put a delete!!!
	delete[] dynamic_ptr;
	dynamic_ptr = NULL; // not necessary, but good practice to avoid accessing invalid memory
	offset_ptr = NULL;

	// Dynamic memory allocation the old-fashioned C way
	dynamic_ptr = (int*) malloc(ARRAY_SIZE*sizeof(int));

	// Now dynamic_ptr behaves exactly as before

	// Dynamic memory freeing, the old-fashioned C way
	free(dynamic_ptr);

	// 'MULTIDIMENSIONAL' ARRAYS
	// -------------------------

	// In C arrys, a multidimensional array is an array of pointers to arrays
	int** multi_array; // pointer to a pointer to an int (read type definitions backwards)

	multi_array = new int*[ARRAY_SIZE]; // allocate array of pointers to ints, note the * after int

	// Allocate a 1D array of ints to each of these pointers
	for(int i = 0; i < ARRAY_SIZE; ++i)
		multi_array[i] = new int[ARRAY_SIZE];

	// Now we have a full 2D array, put some values in
	for(int i = 0; i < ARRAY_SIZE; ++i)
		for(int j = 0; j < ARRAY_SIZE; ++j)
			multi_array[i][j] = i*ARRAY_SIZE + j;

	// Output the array	
	cout << "The multidimensional array is:" << endl;
	for(int i = 0; i < ARRAY_SIZE; ++i)
	{
		for(int j = 0; j < ARRAY_SIZE; ++j)
			cout << multi_array[i][j] << " ";
		cout << endl;
	}

	// Notice how if we dereference multi_array just once, we get a pointer to an int (int*)
	cout << endl << "The array of pointers is " << endl;
	for(int i = 0; i < ARRAY_SIZE; ++i)
		cout << multi_array[i] << " ";
	cout << endl << endl;

	// Delete the 2D array
	// First we have to delete each 1D int array, then finally the pointer array
	for(int i = 0; i < ARRAY_SIZE; ++i)
		delete[] multi_array[i];
	delete[] multi_array;

	// Pointers can go back arbitrarily far
	int********** c_ptr_ptr_ptr_ptr_ptr_ptr_ptr_ptr_ptr_ptr;

	// POINTER CASTING AND VOID*s 
	// --------------------------

	// You can have a pointer to an address without a type
	void* v_ptr;

	// You can make it point to anything
	v_ptr = &x;

	// But you can't dereference it without casting 
	// The following line wouldn't compile if I uncommented it
	// *v_ptr = int(45); 

	// However you can dereference it after casting to a specific type of pointer
	*(int*) v_ptr = 45; // read this backwards: take v_ptr, cast it to be a pointer to an int, then dereference this 

	// There's nothing checking that you do the casting correctly
	cout << "*(int*) v_ptr = " << *(int*) v_ptr << endl;
	cout << "*(float*) v_ptr = " << *(float*) v_ptr << endl; // this will interpret the 4-byte bit pattern at the address v_ptr as a float rather than an int

	// ...even if the two things are different sizes!
	 cout << "*(double*) v_ptr = " << *(double*) v_ptr << endl; // this will interpret the 8-byte bit pattern at the address v_ptr as a double rather than an int
	                                                            // this goes beyond the memory that represents the int that is actually stored there and into whatever follows it (which will in fact be y)

	 // A pointer can be passed to (and indeed returned from) a function, just like any other type
	 x = 6;
	 a_ptr = &x;
	 doubleAnInt(a_ptr);
	 cout << endl << "x = " << x << endl;

	return 0;

}

void doubleAnInt(int* anInt)
{
	*anInt = 2*(*anInt); // double the integer at the pointer
}