
#include <cmath>
#include <iostream>
#include <vector>
#include <algorithm>

void CStyleInPlaceLog( double *array, const unsigned int &length );
void InPlaceLogByReference( std::vector<double> &vector );
void OutOfPlaceLogByReference( const std::vector<double> &in, std::vector<double> &out );
std::vector<double> LogByReturn(const std::vector<double> &in);

int main()
{

  // CStyleInPlaceLog
  const unsigned int length = 5;
  double *array;
  array = new double[length];
  for (unsigned int i = 0; i < length; ++i)
    array[i] = i+1;
  CStyleInPlaceLog(array,length);
  for (unsigned int i = 0; i < length; ++i)
    std::cout << array[i] << " ";
  std::cout << std::endl;  

  delete[] array;

  // InPlaceLogByReference
  std::vector<double> vector = {1, 2, 3, 4, 5 };
  InPlaceLogByReference( vector );
  for (auto element : vector) std::cout << element << " ";
  std::cout << std::endl;

  vector = {1, 2, 3, 4, 5};
  std::vector<double> vector2;
  OutOfPlaceLogByReference( vector, vector2 );
  for (auto element : vector2) std::cout << element << " ";
  std::cout << std::endl;

  vector = {1, 2, 3, 4, 5};
  auto vector3 = LogByReturn(vector);
  for (auto element : vector3) std::cout << element << " ";
  std::cout << std::endl;

  return EXIT_SUCCESS;
}

void CStyleInPlaceLog( double *array, const unsigned int &length )
{
  for (unsigned int i = 0; i < length; ++i)
    array[i] = log(array[i]);
}

void InPlaceLogByReference( std::vector<double> &vector )
{

  for (auto &element : vector) element = log(element);

}

void OutOfPlaceLogByReference( const std::vector<double> &in, std::vector<double> &out )
{

  for (auto element : in)
    out.emplace_back(log(element));

}

std::vector<double> LogByReturn(const std::vector<double> &in)
{

  std::vector<double> out;
  for (auto element : in)
    out.emplace_back( log(element) );
  return out;

}

