#include <iostream>
#include <assert.h>

namespace dv
{
template< typename TReal >
class Triangle
{

public:

Triangle() : Base(0), Height(0)
{
  static_assert(std::is_floating_point<TReal>::value,
                "The type must be floating point.");
};

Triangle(TReal _Base, TReal _Height) : Base(_Base), Height(_Height)
{
  static_assert(std::is_floating_point<TReal>::value,
                "The type must be floating point.");
};

void SetBase(TReal _Base)
{
  assert(_Base >= 0);
  this->Base = _Base;
}

void SetHeight(TReal _Height)
{
  assert(_Height >= 0);
  this->Height = _Height;
}

TReal GetBase() { return this->Base; };
TReal GetHeight() { return this->Height; };

TReal Area()
{
  return 0.5*this->Base*this->Height;
}

private:

TReal Base;
TReal Height;

};
}

int main()
{

  dv::Triangle<float> tri(10,15);
  tri.SetBase(30);

  std::cout << tri.Area() << std::endl;

}

