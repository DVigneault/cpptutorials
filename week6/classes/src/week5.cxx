#include <iostream>
#include <assert.h>

namespace dv
{
template< typename TReal >
class Triangle
{

// Remember that the default for class is private!
public:

// The default constructor
// Use static_assert to make sure the type is floatingpoint
Triangle() : Base(0), Height(0)
{
  static_assert(std::is_floating_point<TReal>::value,
                "The type must be floating point.");
};

// A custom constructor
// Additionally check that the supplied values are reasonable
Triangle(TReal _Base, TReal _Height) : Base(_Base), Height(_Height)
{
  static_assert(std::is_floating_point<TReal>::value,
                "The type must be floating point.");
  assert(_Base >= 0);
  assert(_Height >= 0);
};

// Getters and setters
// By using these, we can enforce restrictions on our data
// For example, it doesn't make sense for Base or Height to
// be negative.
void SetBase(TReal _Base)
{
  assert(_Base >= 0);
  this->Base = _Base;
}

void SetHeight(TReal _Height)
{
  assert(_Height >= 0);
  this->Height = _Height;
}

TReal GetBase() { return this->Base; };
TReal GetHeight() { return this->Height; };

// Function to compute the area
TReal Area()
{
  return 0.5*this->Base*this->Height;
}

private:

// "Backing" variables (the actual data storage)
// Here we've chosen to make the private, and force
// setting through Getters and Setters
// However, it would also be reasonable to expose these,
// so that you had one "safe" way and one "fast" way.
TReal Base;
TReal Height;

};
}

int main()
{

  // And now, you harvest the fruits of your labo(u)r.
  // I present to you: a triangle.

  // |\
  // | \
  // |  \
  // |   \
  // |    \
  // |_____\

  dv::Triangle<float> tri(10,15);
  tri.SetBase(30);

  std::cout << tri.Area() << std::endl;

}

