
#include <cstdlib>
#include <iostream>
#include <assert.h>

namespace dv
{
template< typename TReal >
class Triangle
{

//typedef float TReal;

public:

Triangle() : Base(0), Height(0)
{
  static_assert(std::is_floating_point<TReal>::value,
  "Not Floating Point");
}

Triangle(TReal _Base, TReal _Height) : Base(_Base), Height(_Height)
{
  assert(_Base >=0);
  assert(_Height >=0);
  static_assert(std::is_floating_point<TReal>::value,
  "Not Floating Point");
}

TReal GetBase()
{
  return this->Base;
}
void SetBase(TReal _Base)
{
  assert(_Base >=0);
  this->Base = _Base;
}
TReal GetHeight()
{
  return this->Height;
}
void SetHeight(TReal _Height)
{
  assert(_Height >=0);
  this->Height = _Height;
}

TReal Area()
{
  return 0.5*this->Base*this->Height;
}

private:

TReal Base;
TReal Height;


};

}

int main()
{

  dv::Triangle<double> tri;
  tri.SetHeight(5);
  tri.SetBase(5);
  std::cout << tri.Area() << std::endl;

  return EXIT_SUCCESS;
}
