#include <cstdlib>
#include <iostream>
#include <string>
#include <bitset>

int main()
{

  short x = 56;
  short y = 7;

  // Arithmetic, binary operators
  std::cout << "x+y : " << x + y << std::endl;
  std::cout << "x-y : " << x - y << std::endl;
  std::cout << "x*y : " << x * y << std::endl;
  std::cout << "x/y : " << x / y << std::endl; // Casting!
  std::cout << "x%y : " << x % y << std::endl << std::endl; // May not do what you want for negative numbers

  // Compound operators
  std::cout << "x+=5 : " << (x += 5) << std::endl; // Precedence!
  std::cout << "x-=5 : " << (x -= 5) << std::endl;
  std::cout << "x*=5 : " << (x *= 5) << std::endl;
  std::cout << "x/=5 : " << (x /= 5) << std::endl;
  std::cout << "x%=5 : " << (x %= 5) << std::endl << std::endl;

  // Prefix and postfix operators
  std::cout << "++x : " << ++x << std::endl;
  std::cout << "x++ : " << x++ << std::endl; // Prefix vs postfix!
  std::cout << "--x : " << --x << std::endl;
  std::cout << "x-- : " << x-- << std::endl << std::endl;

  // Comparison operators
  std::cout << "x> y : " << (x > y) << std::endl;
  std::cout << "x< y : " << (x < y) << std::endl;
  std::cout << "x==y : " << (x == y) << std::endl;
  std::cout << "x!=y : " << (x != y) << std::endl;
  std::cout << "x<=y : " << (x <= y) << std::endl;
  std::cout << "x>=y : " << (x >= y) << std::endl << std::endl;

  // Ternary operator
  std::cout << "Ternary: " << x << std::endl << std::endl;

  bool t = true;
  bool f = false;

  // Logical operators
  std::cout << "!t     : " << !t << std::endl; 
  std::cout << "t && f : " << (t && f) << std::endl; 
  std::cout << "t || f : " << (t || f) << std::endl << std::endl;

  // Bits
  std::bitset<4> foo = 0b1110;
  std::bitset<4> bar = 0b0111;

  std::cout << "foo : " << foo << std::endl;
  std::cout << "bar : " << bar << std::endl;
  
  std::cout << "foo << 2 : " << (foo << 2) << std::endl;
  std::cout << "foo >> 2 : " << (foo >> 2) << std::endl << std::endl;

  std::cout << "~foo      : " << ~foo << std::endl;
  std::cout << "foo & bar : " << (foo & bar) << std::endl;
  std::cout << "foo | bar : " << (foo | bar) << std::endl;
  std::cout << "foo ^ bar : " << (foo ^ bar) << std::endl << std::endl;

  std::cout << "foo &= bar : " << (foo &= bar) << std::endl;
  std::cout << "foo |= bar : " << (foo |= bar) << std::endl;
  std::cout << "foo ^= bar : " << (foo ^= bar) << std::endl << std::endl;

  unsigned short positive = 1;               // ... 0001
  std::cout << "positive << 0 : " << (positive << 0) << std::endl; // ... 0010
  std::cout << "positive << 1 : " << (positive << 1) << std::endl; // ... 0010
  std::cout << "positive << 2 : " << (positive << 2) << std::endl; // ... 0100

  signed short negative = -1;                 // 1111 1111
  std::cout << (negative << 0) << std::endl;  // ... 1111
  std::cout << (negative << 1) << std::endl;  // ... 1110
  // Signed representation in binary!
  // Undefined vs Unspecified vs Implementation Dependent
  std::cout << (negative >> 1) << std::endl;  // ?

  return EXIT_SUCCESS;

}
