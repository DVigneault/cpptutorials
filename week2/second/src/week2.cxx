#include <cstdlib>
#include <iostream>
#include <vector>

int
main()
{

  // Simple if
  const bool PEOPLE_CAN_FLY = false;
  const bool SUPERMAN_CAN_FLY = true;

  if (PEOPLE_CAN_FLY) std::cout << "I'm not flying." << std::endl;
  if (SUPERMAN_CAN_FLY) std::cout << "Up, up, and away!" << std::endl << std::endl;

  const bool JOHN_IS_SUPERMAN = false;

  // For simple statements, the ternary operator can be useful
  const bool JOHN_CAN_FLY = (JOHN_IS_SUPERMAN && SUPERMAN_CAN_FLY) ? true : false;

  // Multiline if
  if (JOHN_CAN_FLY)
    {
    std::cout << "John is Superman." << std::endl;
    std::cout << "John can fly." << std::endl << std::endl;
    }
  else std::cout << "John is probably an alien." << std::endl << std::endl;

  // Traditional for
  for (unsigned int i = 1; i <= 5; ++i) std::cout << i << std::endl;
  std::cout << std::endl;

  // Range for (c++11)
  std::vector<unsigned int> v = {1, 2, 3, 4, 5};
  for (unsigned int item : v) std::cout << item << std::endl;
  std::cout << std::endl;

  {
    // You can do the same thing with a while loop
    int i = 1;
    while (i <= 5)
      {
      std::cout << i << std::endl;
      ++i;
      }
  std::cout << std::endl;
  }

  {
    // And you can evaluate the condition afterwards
    int i = 10;
    do
      {
      std::cout << i << std::endl;
      ++i;
      }
    while (i <= 5);
  std::cout << std::endl;
  }

  {
    // You can break out of a loop
    int sum = 0;
    while (true)
      {
      sum += rand() % 10 + 1;
      std::cout << "Sum: " << sum << std::endl;
      if (sum >= 100) break;
      }
  std::cout << std::endl;
  }

  {
    // Or skip to the next iteration
    int sum = 0;
    for (unsigned int i = 0; i <= 10; ++i)
      {
      if (0 != i % 2) continue;
      sum += i;
      }
    std::cout << sum << std::endl;
  std::cout << std::endl;
  }

  // Switches
  char letter = 'a';
 
  switch (letter)
    {
    case 'a': case 'e': case 'i': case 'o': case 'u':
      {
      int integer = 25;
      std::cout << "Vowel." << integer << std::endl;
      break;
      }
    case 'y':
      std::cout << "Undefined." << std::endl;
      break;
    default:
      std::cout << "Consonant." << std::endl;
      break;
    }
  std::cout << std::endl;

  // try/catch
  try
    {
    std::cout << v.at(100) << std::endl;
    }
  catch (const std::exception &e)
    {
    std::cerr << "Exception: " << e.what() << std::endl;
    }
  std::cout << std::endl;

  // goto is funky
  // I've never used it, or seen it used in real code
  // I've only encountered it in examples of the great evil that can be done by it
  bool GoToChina = false;
  WORK:
    {
    std::cout << "Work..." << std::endl;
    if (GoToChina) goto CHINA;
    }

  goto HOME;

  BOSTON:
    {
    std::cout << "Boston..." << std::endl;
    goto WORK;
    }

  HOME:
    {
    GoToChina = true;
    std::cout << "Home..." << std::endl;
    goto BOSTON;
    }

  CHINA: return EXIT_SUCCESS;

  return EXIT_FAILURE;

}
