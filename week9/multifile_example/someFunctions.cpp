// This file contains the implementations for the very basic arithmetic functions

int doubleMe(int x)
{
	return 2*x;
}

int tripleMe(int x)
{
	return 3*x;
}