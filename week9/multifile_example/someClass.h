// This is an example class header file - it contains 
// just the information needed to interface to the class
// and nothing else. In particular there is no code that
// compile to any instructions

#ifndef SOMECLASS_H
#define SOMECLASS_H // don't forget the include guard!!!

#include <vector> // need this to parse the header

// Class definition
class someClass
{
	public:
		someClass(); // default constructor
		someClass(std::vector<int> someArgument); // overloaded constructor
		void someMethod(); // example method

	private:
		std::vector<int> someData;
};

#endif 