// This file is the header file for 
// a list of (extremely!) basic arithmetic operations

#ifndef SOMEFUNCTIONS_H
#define SOMEFUNCTIONS_H

// Prototypes
int doubleMe(int x);
int tripleMe(int x);

#endif
