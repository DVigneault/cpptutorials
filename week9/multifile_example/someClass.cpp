// This file contains the actual implementation of the class's methods 

#include "someClass.h" // include own header file
#include <iostream> // need this in the implementation but not to parse the header
#include "someFunctions.h" // need this in the implementation but not to parse the header

// Safe to do this here because it won't affect other files
using namespace std;

// Default constructor
someClass::someClass()
{

}

// Overloaded constructor
someClass::someClass(vector<int> someArgument)
: someData(someArgument)
{

}

// A method - outputs contents of vector to console
void someClass::someMethod()
{
	// Loop over elements of data vector
	// and output them (doubled)
	for(auto someElement : someData)
		cout << doubleMe(someElement) << " ";
	cout << endl;

}