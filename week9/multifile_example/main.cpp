// This file contains the 'main'
// function

#include <iostream> // standard library header file 
#include <vector> // standard library header file
#include <opencv2/core/core.hpp> // opencv header file

// Include our header files for the class and the functions
#include "someClass.h"
#include "someFunctions.h"

// Safe to do this here
using namespace std;


int main()
{
	vector<int> myVec;

	for(int i = 0; i < 5; ++i)
		myVec.push_back(i);

	someClass myObject(myVec);
	myObject.someMethod();

	cout << "Triple 8 is " << tripleMe(8) << endl << endl;

	cv::Mat myMat = cv::Mat::zeros(3,3,CV_8UC1);
	cout << "My matrix is " << endl << myMat << endl;
}