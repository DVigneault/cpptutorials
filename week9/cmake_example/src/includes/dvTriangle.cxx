
#ifndef dvTriangle_cxx
#define dvTriangle_cxx

#include "dvTriangle.h"
#include <assert.h>
#include <type_traits>

namespace dv
{

template< typename TReal >
Triangle<TReal>
::Triangle() : Base(0), Height(0)
{
  static_assert(std::is_floating_point<TReal>::value,
                "The type must be floating point.");
}

template< typename TReal >
Triangle<TReal>
::Triangle(TReal _Base, TReal _Height) : Base(_Base), Height(_Height)
{
  static_assert(std::is_floating_point<TReal>::value,
                "The type must be floating point.");
  assert(_Base >= 0);
  assert(_Height >= 0);
}

template< typename TReal >
void
Triangle<TReal>
::SetBase(TReal _Base)
{
  assert(_Base >= 0);
  this->Base = _Base;
}

template< typename TReal >
void
Triangle<TReal>
::SetHeight(TReal _Height)
{
  assert(_Height >= 0);
  this->Height = _Height;
}

template< typename TReal >
TReal
Triangle<TReal>
::GetBase() { return this->Base; };

template< typename TReal >
TReal
Triangle<TReal>
::GetHeight() { return this->Height; };

template< typename TReal >
TReal
Triangle<TReal>
::Area()
{
  return 0.5*this->Base*this->Height;
}

} // namespace

#endif
