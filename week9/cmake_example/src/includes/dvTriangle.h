#ifndef dvTriangle_h
#define dvTriangle_h

namespace dv
{
template< typename TReal >
class Triangle
{

public:

  // Constructors
  Triangle();
  Triangle(TReal,TReal);

  // Setters
  void SetBase(TReal);
  void SetHeight(TReal);

  // Getters
  TReal GetBase();
  TReal GetHeight();
  
  // Function to compute the area
  TReal Area();
  
private:

  // Backing data
  TReal Base;
  TReal Height;

};
}

#include "dvTriangle.cxx"

#endif

