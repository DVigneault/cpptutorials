#include <cstdlib>

// Including your own headers
#include <iostream>
#include <dvTriangle.h>

// Including library headers
#include <itkImageFileReader.h>
#include <itkStatisticsImageFilter.h>

// Including library headers
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

int main()
{

  dv::Triangle<double> tri(25,35);
  std::cout << "Triangle Area: " << tri.Area() << std::endl;

  std::string fileName
    = "/Users/Davis/Developer/Bitbucket/cpptutorials/week9/cmake_example/data/cameraman.png";
  typedef itk::Image<float,2> TImage;
  typedef itk::ImageFileReader< TImage > TReader;
  typedef itk::StatisticsImageFilter< TImage > TStatistics;
  auto reader = TReader::New();
  auto stats = TStatistics::New();
  reader->SetFileName( fileName );
  stats->SetInput( reader->GetOutput() );
  stats->Update();
  std::cout << "Cameraman Mean: " << stats->GetMean() << std::endl;

  cv::Mat image = cv::imread( fileName, CV_LOAD_IMAGE_COLOR );
  cv::imshow( "Cameraman", image);
  cv::waitKey(0);

  return EXIT_SUCCESS;

}
