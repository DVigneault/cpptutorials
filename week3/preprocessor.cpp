#include <cstdlib>
#include <iostream>
//#include "chrisDefines.h"

#ifndef CHRIS_FAVOURITE_NUMBER
	#define CHRIS_FAVOURITE_NUMBER 42
#endif

int main()
{
	#if CHRIS_FAVOURITE_NUMBER > 20
		int num = CHRIS_FAVOURITE_NUMBER;
	#else
		float num = CHRIS_FAVOURITE_NUMBER;
	#endif

	std::cout << "Chris's favourite number is " << CHRIS_FAVOURITE_NUMBER << std::endl;

	#ifdef CHRIS_DEFINES_H
	std::cout << "Davis's favourite number is " << Davis_Favourite_Number << std::endl;
	#endif

	// Defines can take arguments
	// Then they are called "macros"
	#define MAX(a,b) ((a)>(b)?(a):(b))
	std::cout << "MAX(7,4) = " << MAX(7,4) << std::endl;

	// But they don't EVALUATE their arguments
	#define MULTIPLY(x,y) x*y
	std::cout << "MULTIPLY(2,3) = " << MULTIPLY(2,3) << std::endl; // 2 * 3 = 6?    
	std::cout << "MULTIPLY(2+3,2+3) = " << MULTIPLY(2+3,2+3) << std::endl; // 5 * 5 = 25? 
	std::cout << "100.0 / MULTIPLY(2+3,2+3) = " << 100.0 / MULTIPLY(2+3,2+3) << std::endl; // 100 / 25 = 4?
	std::cout << std::endl;

	// Predefined macros, useful for debugging
	std::cout << __LINE__ << std::endl;
	std::cout << __FILE__ << std::endl;
	std::cout << __DATE__ << std::endl;
	std::cout << __TIME__ << std::endl;

	// In fact, this is a #define!
	return EXIT_SUCCESS;
}
