#include <iostream> // cin, cout, endl
#include <cstdlib>  // EXIT_SUCCESS
#include <cmath>    // M_PI, fmod
#include <string>

//#define SAY_GOODBYE


// Convert input angle to radians
// Return double
double deg2rad(double ang)
{
	double local_x;

	local_x = ang*M_PI/180.0;
	return local_x;
}

// Convert input angle to degrees
// Return double
double rad2deg(double ang)
{
	return ang*180.0/M_PI;
}

// Wrap an angle to the range 0 to 2pi or 0 to 360
double wrapAngle(double x, bool usingDegrees = false)
{
	if(usingDegrees)
		return fmod(x,360.0); // fmod is "float modulo"
	else
		return fmod(x,2.0*M_PI); 
}


// This just a prototype for a function that is written after main
void wrapAngle_by_reference(double &x, bool usingDegrees = false);

// The main function - this is still what is called first!
// Asks the user for an angle, performs some operations on it 
// and outputs the result
int main(void) // Optional void in inputs
{
	double theta;
	
	// Get an input from the user
	std::cout << "Enter a valid angle in radians" << std::endl << ">> "; 
	while(!(std::cin >> theta))
	{
		std::cin.clear(); // clear the error flag 
		std::string astring;
		std::cin >> astring; // gets rid of whatever was in the cin buffer
		std::cout << "You did it wrong, " << astring << " isn't an angle! Try again!" << std::endl << ">> "; 
	} 

	// Convert to degrees and output 
	double theta_degrees = rad2deg(theta);
	std::cout << "The angle in degrees is " << theta_degrees << std::endl;

	// Use the overloaded function 
	double theta_wrapped = wrapAngle(theta);
	std::cout << "The wrapped angle is " << theta_wrapped << std::endl;

	// Wrap the angle in degrees in place
	wrapAngle_by_reference(theta_degrees,true);
	std::cout << "The wrapped angle in degrees is " << theta_degrees << std::endl;

	return EXIT_SUCCESS;

}

// Pass by reference
// The value of x *WILL* change in the calling function
void wrapAngle_by_reference(double &x, bool usingDegrees)
{
	if(usingDegrees)
		x = fmod(x,360.0);
	else
		x = fmod(x,2.0*M_PI);

	// No return statement here - function is void
}
