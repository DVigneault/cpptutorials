#include <cstdlib>
#include <iostream>

///////////////////////////
// Sequential containers //
///////////////////////////

// Order depends on the order in which they were inserted

// Flexible-sized array
// Fast random access
// Inserting/deleting elements is usually
// faster at the end than at the beginning
#include <vector>

// Fixed-sized array
// Fast random access
#include <array>

// Doubly linked list
// Inserting/deleting is fast anywhere
// Bidirectional sequential access
#include <list>

////////////////////////////
// Associative Containers //
////////////////////////////

// Depends (in some way) on the value of the elements

// Contains only keys
// Keys may not be repeated
#include <set>

// Contains key-value pairs
// Keys may not be repeated, but values may
// Called a Dictionary in some other languages
#include <map>

// Additionally add the present directory
#include "includes/dvFunctions.cpp"

// Replacing <> with "" generally works
// Replacing "" with <> doesn't necessarily

int main()
{

  typedef double StorageType;
  const unsigned char Size = 8;

  std::cout << "///////////////////" << std::endl;
  std::cout << "// C-Style Array //" << std::endl;
  std::cout << "///////////////////" << std::endl << std::endl;

  StorageType c_array[Size]{0, 1, 2, 3, 4, 5, 6, 7};
  std::cout << "sizeof(): " << sizeof(c_array) << std::endl; // 64
  std::cout << "c_array[0]: " << c_array[0] << std::endl; // 0
//  std::cout << "c_array[-1]: " << c_array[-1] << std::endl; // ?
//  std::cout << "c_array[8]: " << c_array[8] << std::endl; // ?
  for (unsigned int i = 0; i < Size; ++i)
    std::cout << c_array[i] << " ";
  std::cout << std::endl << std::endl;

  std::cout << "////////////////" << std::endl;
  std::cout << "// std::array //" << std::endl;
  std::cout << "////////////////" << std::endl << std::endl;

  std::array<StorageType, Size> a{0, 1, 2, 3, 4, 5, 6, 7};
  std::cout << "Size: " << sizeof(a) << std::endl;
  std::cout << "a[0]: " << a[0] << std::endl; // No bounds checking
  std::cout << "a.at(0): " << a.at(0) << std::endl; // Bounds checking
//  std::cout << "a.at(8): " << a.at(8) << std::endl;
  std::cout << "front(): " << a.front() << std::endl;
  std::cout << "back(): " << a.back() << std::endl;
  std::cout << "data(): " << a.data() << std::endl;
  std::cout << "empty(): " << a.empty() << std::endl;
  std::cout << "size(): " << a.size() << std::endl;

//  for (std::array<StorageType, Size>::const_iterator it = a.cbegin();
  for (auto it = a.cbegin();
       it != a.cend();
       ++it)
    std::cout << *it << " ";
  std::cout << std::endl << std::endl;

  std::cout << "/////////////////" << std::endl;
  std::cout << "// std::vector //" << std::endl;
  std::cout << "/////////////////" << std::endl << std::endl;

  std::vector<StorageType> v{0, 1, 2, 3, 4, 5, 6, 7};
  std::cout << "sizeof(): " << sizeof(v) << std::endl;
//  for (unsigned int i = 0; i < 100; ++i) v.push_back(100);
  for (unsigned int i = 0; i < 100; ++i) v.emplace_back(100);
  std::cout << "sizeof(): " << sizeof(v) << std::endl;
 
  std::cout << "size(): " << v.size() << std::endl;
  std::cout << "capacity(): " << v.capacity() << std::endl;
  std::cout << "shrinking..." << std::endl;
  v.shrink_to_fit();
  std::cout << "size(): " << v.size() << std::endl;
  std::cout << "capacity(): " << v.capacity() << std::endl;
  std::cout << std::endl;

  std::cout << "///////////////" << std::endl;
  std::cout << "// std::list //" << std::endl;
  std::cout << "///////////////" << std::endl << std::endl;

  // Does NOT have at(), data()
  std::list<StorageType> l{0, 1, 2, 3, 4, 5, 6, 7};
//  for (unsigned int i = 20; i < 30; ++i) l.push_front(i);
  for (unsigned int i = 20; i < 30; ++i) l.emplace_front(i);
  for (auto it = l.cbegin(); it != l.cend(); ++it) std::cout << *it << " ";
  std::cout << std::endl;

//  for (unsigned int i = 25; i < 35; ++i) l.push_back(i);
  for (unsigned int i = 25; i < 35; ++i) l.emplace_back(i);
  for (auto it = l.cbegin(); it != l.cend(); ++it) std::cout << *it << " ";
  std::cout << std::endl;

  l.reverse();
  for (auto it = l.cbegin(); it != l.cend(); ++it) std::cout << *it << " ";
  std::cout << std::endl;

  l.sort();
  for (auto it = l.cbegin(); it != l.cend(); ++it) std::cout << *it << " ";
  std::cout << std::endl;

  l.unique();
  for (auto it = l.cbegin(); it != l.cend(); ++it) std::cout << *it << " ";
  std::cout << std::endl << std::endl;

  std::cout << "//////////////" << std::endl;
  std::cout << "// std::set //" << std::endl;
  std::cout << "//////////////" << std::endl << std::endl;

  std::set<std::string> s{"Z", "Y", "X", "A", "B", "C", "D", "D", "D", "D"};
  for (auto it = s.cbegin(); it != s.cend(); ++it) std::cout << *it << " ";
  std::cout << std::endl << std::endl;

  std::cout << "//////////////" << std::endl;
  std::cout << "// std::map //" << std::endl;
  std::cout << "//////////////" << std::endl << std::endl;
 
  std::map<std::string, std::string> m{ { "James", "Joyce" },
                                        { "James", "Baldwin" },
                                        { "Joan", "Didion" },
                                        { "Flannery", "O'Connor" } };

  for (auto it = m.cbegin(); it != m.cend(); ++it)
    std::cout << it->first << " " << it->second << std::endl;
  std::cout << std::endl;

  m.insert( { "Dennis", "Johnson" } );
  m.insert( { "James", "Baldwin" } );

  for (auto it = m.cbegin(); it != m.cend(); ++it)
    std::cout << it->first << " " << it->second << std::endl;
  std::cout << std::endl;

  m["James"] = "Baldwin";
  m["Tennessee"] = "Williams";
  auto x = m["Lewis"];
  std::cout << x << std::endl;

  for (auto it = m.cbegin(); it != m.cend(); ++it)
    std::cout << it->first << " " << it->second << std::endl;
  std::cout << std::endl;

  std::cout << "///////////////" << std::endl;
  std::cout << "// Templates //" << std::endl;
  std::cout << "///////////////" << std::endl << std::endl;

  std::cout << "Array: " << std::endl;
  dv::PrintContainerToScreen( a );
  std::cout << std::endl;

  std::cout << "Vector: " << std::endl;
  dv::PrintContainerToScreen( v );
  std::cout << std::endl;

  std::cout << "List: " << std::endl;
  dv::PrintContainerToScreen( l );
  std::cout << std::endl;

  std::cout << "Set: " << std::endl;
  dv::PrintContainerToScreen( s );
  std::cout << std::endl;
  
//  std::cout << "Map: " << std::endl;
//  dv::PrintContainerToScreen( m );
//  std::cout << std::endl;

  return EXIT_SUCCESS;

}

