#ifndef dv_Functions_cpp
#define dv_Functions_cpp

namespace dv
{

template< typename T >
void PrintContainerToScreen( const T &container )
{

for (auto it = container.cbegin(); it != container.cend(); ++it)
  std::cout << *it << " ";

std::cout << std::endl;

}

} // End namespace

#endif
