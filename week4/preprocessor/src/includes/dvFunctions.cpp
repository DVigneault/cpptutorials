#ifndef dv_Functions_cpp
#define dv_Functions_cpp

namespace dv
{

void swap(double &a, double &b)
{
  double temp = a;
  a = b;
  b = temp;
}

} // End namespace

#endif
