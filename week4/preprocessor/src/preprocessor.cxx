// Library includes
// Search directories designated by the compiler/IDE
#include <cstdlib>
#include <iostream>

// Additionally add the present directory
#include "includes/dvFunctions.cpp"

// Replacing <> with "" generally works
// Replacing "" with <> doesn't necessarily

int main()
{

// Defining constants
const unsigned int length = 3;
#define LENGTH 3

const float array1[length] = {0, 1, 2};
const float array2[LENGTH] = {0, 1, 2};

for (unsigned int i = 0; i < length; ++i)
  {
  std::cout << array1[i] << std::endl;
  std::cout << array2[i] << std::endl;
  }
  std::cout << std::endl;

// Defines can take arguments
#define MAX(a,b) ((a)>(b)?(a):(b))
std::cout << MAX(7,4) << std::endl;

// But they don't EVALUATE their arguments
#define MULTIPLY(x,y) x*y
std::cout << MULTIPLY(2,3) << std::endl; // 2 * 3 = 6?
std::cout << MULTIPLY(2+3,2+3) << std::endl; // 5 * 5 = 25?
std::cout << 100.0 / MULTIPLY(2+3,2+3) << std::endl; // 100 / 25 = 4?
std::cout << std::endl;

// Predefined macros, useful for debugging
std::cout << __LINE__ << std::endl;
std::cout << __FILE__ << std::endl;
std::cout << __DATE__ << std::endl;
std::cout << __TIME__ << std::endl;

return EXIT_SUCCESS;

}
