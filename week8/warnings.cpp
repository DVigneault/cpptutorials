#include <random>
#include <iostream>

// This file demonstrates compiler warnings that may arise 
// Try compling it with different levels of warnings:
// With g++ -w -Wall -Wextra -Werror
// With Visual Studio /w /W1 /W2 /W3 /W4 /Wall /WX

int a_func(int i)
{
	if(i > 2)
		return i-1; 
}

int main()
{
	// This bits sets up stuff to enable random number generation 
	// Don't worry about it for now...
	std::random_device rd;
	std::default_random_engine e1(rd());
    std::uniform_int_distribution<int> uniform_dist(1, 6);

	int x = uniform_dist(e1); // generate a random number between 1 and 6
	int y;

	std::cout << "y = " << y << std::endl;

	x = y + 2;

	if(x = y)
		std::cout << "It turns out x = y" << std::endl;

	#warning "Danger - High Voltage"


}