#include <iostream>
#include <vector>
#include <tuple>
//#include <memory>

// I think that this should help do the tuple to something else conversion
// http://stackoverflow.com/questions/4691657/is-it-possible-to-store-a-template-parameter-pack-without-expanding-it

template < template<typename,typename> class A, typename B>
struct type_param
{
	using type = A<B,std::allocator<B>>;
};

typedef type_param<std::vector,int>::type my_type;

// -----------------------------------------------------
// Way to create a specific struct using the template parameters from a tuple


template <typename... TArgs>
struct some_struct
{
	// empty struct
};

template <typename... TArgs>
struct convert_tuple_to_some_struct;

template <typename... TArgs>
struct convert_tuple_to_some_struct<std::tuple<TArgs...>>
{
	// expand the variadic_typedef back into
	// its arguments, via specialization
	// (doesn't rely on functionality to be provided
	// by the variadic_typedef struct itself, generic)
	using type = some_struct<TArgs...>;
};

static_assert(std::is_same< some_struct<int> , typename convert_tuple_to_some_struct<std::tuple<int>>::type >::value, "These types are not the same");

//-------------------------------------------------------------
// Generalising this to any variadic struct

template <template <typename...> class T, typename... TArgs>
struct convert_tuple_to_anything;

template <template <typename...> class T, typename... TArgs>
struct convert_tuple_to_anything<T, std::tuple<TArgs...>>
{
	// expand the variadic_typedef back into
	// its arguments, via specialization
	// (doesn't rely on functionality to be provided
	// by the variadic_typedef struct itself, generic)
	using type = T<TArgs...>;
};

static_assert(std::is_same< some_struct<int> , typename convert_tuple_to_anything<some_struct,std::tuple<int>>::type >::value, "These types are not the same");

//--------------------------------------------------------------
// Generalising this to any variadic type to any other

template <template <typename...> class TToType, template <typename...> class TFromType, typename... TArgs>
struct convert_anything_to_anything;

template <template <typename...> class TToType, template <typename...> class TFromType, typename... TArgs>
struct convert_anything_to_anything<TToType, TFromType, TFromType<TArgs...>>
{
	// expand the variadic_typedef back into
	// its arguments, via specialization
	// (doesn't rely on functionality to be provided
	// by the variadic_typedef struct itself, generic)
	using type = TToType<TArgs...>;
};

static_assert(std::is_same< some_struct<int> , typename convert_anything_to_anything<some_struct,std::tuple,std::tuple<int>>::type >::value, "These types are not the same");

//--------------------------------------------------------------------
// Creating a fully generic type repeater
template <
unsigned N,
template <typename...> class T,
typename Arg,
typename... Args
>
struct type_extend_impl
{
	// This class was invoked with Arg, Args... and to increase the
	// Arg one more time we need to add it again which is why Arg
	// appears twice below...
	using type = typename type_extend_impl<N-1,T,Arg,Arg,Args...>::type;
};

template <
template <typename...> class T,
typename Arg,
typename... Args
>
struct type_extend_impl<0,T,Arg,Args...>
{
	// Base case: Stop the recursion and expose Arg.
	using type = T<Arg,Args...>;
};

template <unsigned N, template <typename...> class T, typename Arg>
struct type_extend
{
	using type = typename type_extend_impl<N-1,T,Arg>::type;
};

template <template <typename...> class T, typename Arg>
struct type_extend<0,T,Arg>
{
	using type =  T<>;
};



template <unsigned N, template <typename...> class T, typename Arg>
using type_extend_type = typename type_extend<N,T,Arg>::type;

static_assert(std::is_same< type_extend_type<4,std::tuple,char> , std::tuple<char,char,char,char> >::value, "These types are not the same");

 //--------------------------------
// A template pack concatenator for a specific type
// From potato swatter's answer on SO http://stackoverflow.com/questions/16648144/merge-two-variadic-templates-in-one

template< typename ... t >
struct type_list {};

template< typename... pack >
struct type_cat_impl;

template< typename ... a, typename ... b >
struct type_cat_impl< type_list< a ... >, type_list< b ... > >
{
	typedef type_list< a ..., b ... > type;
};


static_assert(std::is_same< type_cat_impl<type_list<int,char>,type_list<float,double>>::type , type_list<int,char,float,double> >::value, "These types are not the same");

// ----------------------------------------------
// Generalising this to an arbitrary type

template< template<typename...> class TTemp, typename TA, typename TB >
struct generic_type_cat_impl;

template< template<typename...> class TTemp, typename... TA, typename... TB  >
struct generic_type_cat_impl< TTemp, TTemp< TA ... >, TTemp< TB ... > >
{
	using type = TTemp< TA ..., TB ... >;
};


template< template<typename...> class TTemp, typename TA, typename TB >
using generic_type_cat = typename generic_type_cat_impl<TTemp,TA,TB>::type;

static_assert(std::is_same< generic_type_cat_impl<type_list,type_list<int,char>,type_list<float,double>>::type , type_list<int,char,float,double> >::value, "These types are not the same");
static_assert(std::is_same< generic_type_cat<type_list,type_list<int,char>,type_list<float,double>> , type_list<int,char,float,double> >::value, "These types are not the same");

// ----------------------------------------------------
// Use this as a basis for a recursive scheme to concatenate any number of
// parameter packs

// can't get this to work
/*
template< template<typename...> class TTemp, typename... T>
struct multi_type_cat;

template< template<typename...> class TTemp, typename T>
struct multi_type_cat<TTemp,T>
{
	typedef T type;
};

template< template<typename...> class TTemp, typename TFirst, typename... TRest>
struct multi_type_cat<TTemp,TFirst,TRest...>
{
	typedef generic_type_cat_impl< TTemp, TFirst, multi_type_cat<TTemp,TRest...>::type >::type type ;
};
*/


// ----------------------------------------------------------

int main()
{
	my_type var(5,0);
	for(auto v : var)
		std::cout << v << std::endl;

	typename convert_tuple_to_some_struct<std::tuple<int>>::type x;

	typename convert_tuple_to_anything<some_struct,std::tuple<int>>::type y;

	type_extend_type<4,std::tuple,char> z;
}
